import './Line.css';

export const Line = (props) => {
    return (
        <div className={`Line ${props.addClass || ''}`}>
            {props.children}
        </div>
    )
}

export const Between = (props) => {
    return (
        <Line children={props.children} addClass="Between" />
    )
}

export const Rows = (props) => {
    return (
        <Line children={props.children} addClass={`Rows ${props.addClass || ''}`} />
    )
}

export const RowsFlex = (props) => {
    return (
        <Rows {...props} addClass="flex" />
    )
}