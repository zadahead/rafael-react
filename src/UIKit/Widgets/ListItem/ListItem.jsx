import { Between, Checkbox, Icon, Line } from "UIKit"

import './ListItem.css';

export const ListItem = ({ title, checked, onChecked, onDelete }) => {
    return (
        <div className={`ListItem ${checked ? 'checked' : ''}`}>
            <Between>
                <Line>
                    <Checkbox checked={checked} onChange={onChecked} />
                    <h4 className="title">{title}</h4>
                </Line>
                <Line>
                    {onDelete && (
                        <Icon i="trash" onClick={onDelete} />
                    )}
                </Line>
            </Between>
        </div>
    )
}