import { useFetch } from "Hooks/useFetch";

export const Fetch = ({ url, onLoad }) => {
    const { resp, isLoading, error } = useFetch(url);

    const renderResponse = () => {
        if (error) {
            return <h5 style={{ color: 'red' }}>{error}</h5>
        }

        if (isLoading) {
            return <h5>loading...</h5>
        }
        return onLoad(resp);
    }

    return renderResponse();
}