import { Icon, Line } from "UIKit"

import "./Tree.css";

export const Tree = ({ list, onChange, onItemSelect }) => {



    const handleChange = (item) => {
        const isFolder = item.items;
        if (isFolder) {
            handleToggle(item);
        } else {
            handleItemSelect(item);
        }
    }

    const handleToggle = (item) => {
        item.open = !item.open;
        onChange([...list]);
    }

    const handleItemSelect = (item) => {
        onItemSelect({ ...item });
    }

    const getNode = (id, nodes) => {
        if (nodes?.length) {
            for (let i = 0; i < nodes.length; i++) {
                const node = nodes[i];
                if (node.id === id) {
                    return node;
                }
                const inner = getNode(id, node.items);
                if (inner) {
                    return inner;
                }
            }
        }

        return false;
    }



    return (
        <div className="Tree">
            <TreeChild list={list} onChange={handleChange} />
        </div>
    )
}

const TreeChild = ({ list, level = 1, onChange }) => {
    const renderList = () => {
        return list.map(i => {
            const isFolder = i.items;

            return (
                <div key={i.id}>
                    <div className="item"
                        style={{
                            paddingLeft: `calc(var(--gap) * ${level})`
                        }}
                        onClick={() => onChange && onChange(i)}
                    >
                        <Line>
                            <div style={{ visibility: isFolder ? 'visible' : 'hidden' }}>
                                <Icon
                                    i={`${i.open ? 'angle-down' : 'angle-right'}`}
                                />
                            </div>
                            <h4>{i.title}</h4>
                        </Line>
                    </div>
                    {isFolder && i.open && <TreeChild list={i.items} level={level + 1} onChange={onChange} />}
                </div>
            )
        })
    }

    return renderList();
}