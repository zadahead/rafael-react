import { useEffect, useState } from 'react';
import { Between, Icon } from 'UIKit';
import './Dropdown.css';

export const Dropdown = ({ list, selected, onChange }) => {
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        document.body.addEventListener('click', handleClose);

        return () => {
            document.body.removeEventListener('click', handleClose);
        }
    }, [])

    const handleToggle = (e) => {
        e.stopPropagation();
        setIsOpen(!isOpen);
    }

    const handleSelect = (i) => {
        onChange(i.id);
        setIsOpen(false);
    }

    const handleClose = () => {
        setIsOpen(false);
    }

    const renderList = () => (
        list.map(i => {
            const isSelected = i.id === selected;
            return (
                <div
                    key={i.id}
                    className={`${isSelected ? 'selected' : ''}`}
                    onClick={() => handleSelect(i)}
                >
                    {i.name}
                </div>
            )
        })
    )

    const getTitle = () => {
        if (selected) {
            const item = list.find(i => i.id === selected);
            if (item) {
                return item.name;
            }
        }
        return 'please select';
    }

    return (
        <div className='Dropdown'>
            <div className='header' onClick={handleToggle}>
                <Between>
                    <h4>{getTitle()}</h4>
                    <Icon i="chevron-down" />
                </Between>
            </div>
            {isOpen && (
                <div className='list'>
                    {renderList()}
                </div>
            )}
        </div>
    )
}