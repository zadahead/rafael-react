import { Between, Icon } from 'UIKit';
import './Btn.css';

export const Btn = (props) => {
    return (
        <button
            className='Btn'
            onClick={props.onClick}
            data-testid="btn"
            disabled={props.disabled}
        >
            <Between>
                {props.i && <Icon i={props.i} />}
                {props.children}
            </Between>
        </button>
    )
}