import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Btn } from "./Btn";


describe("<Btn />", () => {
    it("will validate Btn is rendered", () => {
        render(<Btn>Add</Btn>)

        const btn = screen.getByTestId('btn');

        expect(btn).toBeInTheDocument();
    })

    it("will render Btn with icon", () => {
        render(<Btn i="heart">Add</Btn>)

        const icon = screen.getByTestId('icon');

        expect(icon).toBeInTheDocument();
    })

    it("will trigger action onClick", () => {
        const handleClick = jest.fn();

        render(<Btn i="heart" onClick={handleClick}>Add</Btn>)

        const btn = screen.getByTestId('btn');

        fireEvent.click(btn);

        expect(handleClick).toBeCalledTimes(1);
    })
})