import { Rows } from 'UIKit';
import './Box.css';

export const Box = (props) => {
    return (
        <div className="Box">
            <Rows>
                <h2>{props.title}</h2>
                {props.children}
            </Rows>
        </div>
    )
}