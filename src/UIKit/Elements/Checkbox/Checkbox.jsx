import { Icon, Line } from "UIKit"

import './Checkbox.css';

export const Checkbox = (props) => {

    const handleChange = () => {
        props.onChange(!props.checked);
    }

    return (
        <div className="Checkbox" onClick={handleChange} >
            <Line>
                <Icon i={props.checked ? 'check-square' : 'square'} />
                <h5>{props.label}</h5>
            </Line>
        </div>
    )
}