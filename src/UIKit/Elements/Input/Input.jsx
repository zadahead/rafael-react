import { forwardRef } from 'react';

import './Input.css';

export const Input = forwardRef((props, ref) => {
    const { errors, ...rest } = props;

    const handleChange = (e) => {
        const value = e.target.value;
        props.onChange(value);
    }

    return (
        <div className={`Input ${errors.length ? 'error' : ''}`}>
            <input
                ref={ref}
                {...rest}
                onChange={handleChange}
            />
        </div>
    )
})