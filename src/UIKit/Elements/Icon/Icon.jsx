import './Icon.css';

export const Icon = (props) => {
    return (
        <div
            onClick={props.onClick}
            className={`Icon ${props.onClick ? 'clickable' : ''}`}
            data-testid="icon"
        >
            <i className={`fas fa-${props.i}`} ></i>
        </div>
    )
}