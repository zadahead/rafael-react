import { createContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { getUser, isUser, removeUser, setUser } from "./userStorage";

export const userContext = createContext();

const Provider = userContext.Provider;

export const UserProvider = ({ children }) => {
    const [data, setData] = useState(getUser());
    const [isLogin, setIsLogin] = useState(isUser());
    const navigate = useNavigate();

    const logout = () => {
        setData({});
        setIsLogin(false);
        removeUser();
        navigate('/login');
    }

    const toLogin = () => {
        navigate('/login');
    }

    const login = (d) => {
        setData(d);
        setIsLogin(true);
        setUser(d);
    }

    const value = {
        isLogin,
        user: data,
        logout,
        login,
        toLogin
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}