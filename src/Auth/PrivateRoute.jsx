import { useContext } from "react";
import { Navigate } from "react-router-dom";
import { userContext } from "./userContext";

export const PrivateRoute = ({ child }) => {
    const { isLogin } = useContext(userContext);

    if (!isLogin) {
        return (
            <Navigate to="/login" />
        )
    }

    return child;

}