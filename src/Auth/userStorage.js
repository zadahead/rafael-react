const USER_DATA = 'userData';

export const getUser = () => {
    const item = localStorage.getItem(USER_DATA);

    if (item) {
        return JSON.parse(item);
    }

    return {}
}

export const setUser = (user) => {
    localStorage.setItem(USER_DATA, JSON.stringify(user));
}

export const removeUser = () => {
    localStorage.removeItem(USER_DATA);
}

export const isUser = () => {
    const user = getUser();
    return user.token ? true : false;
}