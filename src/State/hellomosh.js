//Actions
export const sayDavid = () => {
    return { type: 'SAY_DAVID' }
}

//Reducers
export const hellomoshReducer = (state = "Hello Mosh", { type }) => {
    switch (type) {
        case "SAY_DAVID": return "Hello David";
        default: return state;
    }
}