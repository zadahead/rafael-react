
//Actions 

export const colorSwitch = () => {
    return {
        type: "COLOR_TOGGLE"
    }
}

//Reducer 

export const colorReducer = (isRed = true, action) => {
    switch (action.type) {
        case "COLOR_TOGGLE": return !isRed;
        default: return isRed;
    }
}