import { createStore, combineReducers, applyMiddleware } from 'redux';
import { countReducer } from "./counter";
import { Provider } from "react-redux";
import { hellomoshReducer } from './hellomosh';
import { colorReducer } from './color';
import { todosReducer } from './todos';
import ReduxThunk from "redux-thunk";

const reducers = combineReducers({
    count: countReducer,
    hello: hellomoshReducer,
    color: colorReducer,
    todos: todosReducer
})

const middlewares = applyMiddleware(ReduxThunk);



const store = createStore(reducers, middlewares);

export const Store = ({ children }) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}

