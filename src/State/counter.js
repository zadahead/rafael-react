
//Actions 
export const addCount = () => {
    return {
        type: "ADD_COUNT",
        payload: 15
    }
}

export const reduCount = (amount) => {
    return {
        type: "REDU_COUNT",
        payload: amount
    }
}

export const multCount = () => {
    return {
        type: "MULT_COUNT",
        payload: 2
    }
}

//Reducers

export const countReducer = (count = 15, action) => {
    const { type, payload } = action;

    switch (type) {
        case "ADD_COUNT": return count + payload;
        case "REDU_COUNT": return count - payload;
        case "MULT_COUNT": return count * payload;
        default: return count;
    }
}