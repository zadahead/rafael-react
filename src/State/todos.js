import axios from "axios";

//Actions
export const getTodos = () => {
    return fetchTodos;
}

const fetchTodos = async (dispatch) => {
    const resp = await axios.get('https://jsonplaceholder.typicode.com/todos');

    dispatch({
        type: "TODOS_GET",
        payload: resp.data.splice(0, 10)
    })
}

export const patchTodos = (id) => {
    return {
        type: 'TODOS_PATCH',
        payload: id
    }
}

export const deleteTodos = (id) => {
    return {
        type: "TODOS_DELETE",
        payload: id
    }
}

export const postTodos = (value) => {
    return {
        type: "TODOS_POST",
        payload: value
    }
}

//Reducers
export const todosReducer = (list = [], action) => {
    switch (action.type) {
        case "TODOS_GET": return action.payload;
        case "TODOS_PATCH": {
            const newList = list.map((i) => {
                if (i.id === action.payload) {
                    i.completed = !i.completed;
                    return i;
                }
                return i;
            });
            return newList;
        }
        case "TODOS_DELETE": {
            const newList = list.filter(i => i.id !== action.payload);
            return newList;
        }
        case "TODOS_POST": {
            const newList = [...list, {
                id: new Date().getMilliseconds(),
                completed: false,
                title: action.payload
            }]

            return newList;
        }
        default: return list;
    }
}