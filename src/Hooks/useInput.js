import { useState } from "react";

export const useInput = (initialState, placeholder) => {
    const [value, setValue] = useState(initialState || '');

    return {
        value,
        onChange: setValue,
        placeholder
    }
}