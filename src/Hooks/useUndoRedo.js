import { useState } from "react";

export const useUndoRedo = (initialValue) => {
    const [stack, setStack] = useState([initialValue]);
    const [current, setCurrent] = useState(0);

    const push = (data) => {
        stack.splice(current + 1, stack.length);

        const newStack = [...stack, data];
        setStack(newStack);
        setCurrent(newStack.length - 1);
    }

    const isUndo = () => {
        return current > 0;
    }

    const undo = () => {
        if (isUndo()) {
            setCurrent(current - 1);
        }
    }

    const isRedo = () => {
        return current < (stack.length - 1)
    }

    const redo = () => {
        if (isRedo()) {
            setCurrent(current + 1);
        }
    }

    return {
        stack,
        push,
        data: stack[current],
        current,
        undo,
        redo,
        isUndo,
        isRedo
    }
}