import { useState } from "react";

export const useSwitcher = () => {
    const [isOn, setIsRed] = useState(true);

    const handleSwitch = () => {
        setIsRed(!isOn);
    }

    return {
        isOn,
        handleSwitch
    }
}