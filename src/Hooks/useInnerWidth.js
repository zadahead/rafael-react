import { useEffect, useState } from "react";

export const useInnerWidth = () => {
    const [width, setWidth] = useState();

    useEffect(() => {
        handleSetWidth();
        window.addEventListener('resize', handleSetWidth);

        return () => {
            window.removeEventListener('resize', handleSetWidth);
        }
    }, [])


    const handleSetWidth = () => {
        setWidth(window.innerWidth);
    }

    return width;
}