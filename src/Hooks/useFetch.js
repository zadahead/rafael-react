import axios from "axios";
import { useEffect, useState } from "react";

export const useFetch = (endpoint) => {
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState('');
    const [resp, setResp] = useState(null);

    useEffect(() => {
        setIsLoading(true);
        setError('');

        const cancelToken = axios.CancelToken.source();

        axios.get(`https://jsonplaceholder.typicode.com${endpoint}`, {
            cancelToken: cancelToken.token
        })
            .then(resp => {
                console.log(resp.data);
                setResp(resp.data);
                setIsLoading(false);
            }).catch(e => {
                if (e.code === 'ERR_CANCELED') {
                    console.log('canceled');
                } else {
                    setIsLoading(false);
                    setError(e.message);
                }
            });

        return () => {
            cancelToken.cancel();
        }
    }, [endpoint])

    return {
        resp,
        isLoading,
        error
    };

}