import { useState } from "react";

export const useValidator = (inputs) => {
    const [isOn, setIsOn] = useState(false);

    const check = () => {
        let isValid = isOn;

        inputs.forEach(block => {
            const [input, validations] = block;

            input.errors = [];

            const push = (msg) => {
                input.errors.push(msg);
                isValid = false;
            }
            if (isOn) {
                validations.forEach(flag => {
                    switch (flag) {
                        case 'empty':
                            if (!input.value) {
                                push("value cannot be empty");
                            }
                            break;
                        case 'email':
                            if (!validateEmail(input.value)) {
                                push("email is required");
                            }
                            break;

                        default:
                            break;
                    }
                });
            }

        });

        return isValid;
    }

    const validate = () => {
        setIsOn(true);
        return check();
    }

    check();

    return validate;
}

const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};