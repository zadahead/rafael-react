import { useState } from "react"
import { Btn, Rows } from "UIKit"

export const Toggler = (props) => {
    const [isShow, setIsShow] = useState(true);

    const handleToggle = () => {
        setIsShow(!isShow);
    }

    const renderComponent = () => {
        if (isShow) {
            return props.children;
        }
        return null;
    }

    return (
        <div>
            <Rows>
                <Btn onClick={handleToggle}>Toggle</Btn>
                {renderComponent()}
            </Rows>
        </div>
    )
}