import { counterContext } from "Context/counterContext";
import { themeContext } from "Context/themeContext";
import { useContext } from "react";
import { Btn, Dropdown, Icon, Rows } from "UIKit"

export const Home = () => {
    const { count } = useContext(counterContext);
    const { list, selected, onChange } = useContext(themeContext);

    const handleClick = () => {
        console.log('hello mosh');
    }

    return (
        <>
            <Rows>
                <Dropdown list={list} selected={selected} onChange={onChange} />


                <Icon i="star" />
                <h2>Hello React App, {count}</h2>

                <Btn i="heart" onClick={handleClick}>Click Me</Btn>

            </Rows>
        </>
    )
}