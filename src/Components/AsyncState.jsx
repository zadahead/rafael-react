import { useRef, useState } from "react"
import { Btn, Rows } from "UIKit"

export const AsyncState = () => {
    const [count, setCount] = useState([0]);
    const countRef = useRef();
    countRef.current = count;

    const handleAdd = () => {
        setCount(count.concat(0));

        setTimeout(() => {

            setCount((state) => {
                console.log('current state', state);
                return state.concat(0);
            });

            console.log(count, countRef);
        }, 3000);
    }

    return (
        <div>
            <Rows>
                <h4>Async State, {count}</h4>
                <Btn onClick={handleAdd}>Add</Btn>
            </Rows>

        </div>
    )
}