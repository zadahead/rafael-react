import { useState } from "react"
import { Btn, Rows } from "UIKit"

export const ColorSwitcher = () => {
    const [color, setColor] = useState('red');

    const handleColorSwitch = () => {
        setColor(color === 'red' ? 'blue' : 'red');
    }

    const styleCss = {
        width: '100px',
        height: '100px',
        backgroundColor: color
    }

    return (
        <div>
            <Rows>
                <div style={styleCss}>box</div>
                <Btn onClick={handleColorSwitch}>Switch</Btn>
            </Rows>
        </div>
    )
}