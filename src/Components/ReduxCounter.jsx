import { useDispatch, useSelector } from "react-redux"
import { addCount, reduceCount } from "Redux/counter";
import { Btn, Rows } from "UIKit"

export const ReduxCounter = () => {
    const count = useSelector((state) => state.counter.value);
    const dispatch = useDispatch();

    const handleAdd = () => {
        dispatch(addCount());
    }

    const handleReduce = () => {
        dispatch(reduceCount())
    }

    return (
        <div>
            <Rows>
                <h3>Count, {count}</h3>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleReduce}>Reduce</Btn>
            </Rows>
        </div>
    )
}