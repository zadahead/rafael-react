import { useState } from "react"
import { Btn, Rows } from "UIKit";

export const Cubes = () => {
    const [list, setList] = useState([]);


    const handleAdd = () => {
        //if you need a new ID, this is where you set it!
        setList(list.concat(Date.now()));
    }

    const handleRemove = (item) => {
        const filteredList = list.filter(i => { return i !== item });
        setList(filteredList);
    }

    const renderList = () => {
        return list.map((i) => {

            const styleCss = {
                height: '50px',
                backgroundColor: 'red'
            }

            return (
                <div
                    key={i}
                    style={styleCss}
                    onClick={() => { handleRemove(i) }}
                >{i}</div>
            )
        })
    }

    return (
        <div>
            <Rows>
                <h4>Cubes list</h4>
                <Btn onClick={handleAdd}>Add</Btn>
                <Rows>
                    {renderList()}
                </Rows>
            </Rows>
        </div>
    )
}