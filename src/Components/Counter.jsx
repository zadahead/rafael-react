import { useState } from "react"
import { Btn, Rows } from "UIKit";

export const Counter = () => {

    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1); //state changed
    }



    //render
    return (
        <div>
            <Rows>
                <h4>Count, {count}</h4>
                <Btn onClick={handleAdd}>Add</Btn>
            </Rows>
        </div>
    )

}