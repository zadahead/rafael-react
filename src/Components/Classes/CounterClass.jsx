import React from "react";
import { Btn, Rows } from "UIKit";

export class CounterClass extends React.Component {
    state = {
        count: 0
    }

    //life cycle
    componentDidMount = () => {
        // console.log('componentDidMount');
    }

    componentDidUpdate = () => {
        //console.log('componentDidUpdate');
    }

    componentWillUnmount = () => {
        // console.log('componentWillUnmount');
    }
    /////

    handleAdd = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render = () => {
        return (
            <div>
                <Rows>
                    <h4>Count, {this.state.count}</h4>
                    <Btn onClick={this.handleAdd}>Add</Btn>
                </Rows>
            </div>
        )
    }
}