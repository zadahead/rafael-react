import { Rows } from "UIKit"

export const Users = (props) => {
    const renderList = () => {
        return props.list.map((item) => {
            return (
                <div key={item.id}>
                    <h5>{item.name}</h5>
                </div>
            )
        })
    }

    return (
        <div>
            <Rows>
                <h3>Users List</h3>
                {renderList()}
            </Rows>
        </div>
    )
}