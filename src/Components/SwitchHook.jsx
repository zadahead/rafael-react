import { useSwitcher } from "Hooks/useSwitcer";
import { Btn, Rows } from "UIKit"



export const SwitcherHook = () => {
    //logic
    const { isOn, handleSwitch } = useSwitcher();

    ///
    const boxStyle = {
        width: '100px',
        height: '100px',
        backgroundColor: isOn ? 'red' : 'blue'
    }

    //render
    return (
        <div>
            <Rows>
                <div style={boxStyle}>

                </div>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </Rows>
        </div>
    )
}