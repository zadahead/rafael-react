import { useCounter } from "Hooks/useCounter";
import { Btn, Rows } from "UIKit";

/*
    1 ) create complete component
    2 ) separate between logic and rend
    3 ) create 'useSwitch' custom hook
    4 ) return the needed values
    5 ) move 'useSwitch' to its folder
*/


export const CounterHook = () => {
    //logic
    const [count, handleAdd] = useCounter();

    //render
    return (
        <div>
            <Rows>
                <h4>Count: {count}</h4>
                <Btn onClick={handleAdd}>Add</Btn>
            </Rows>
        </div>
    )
}