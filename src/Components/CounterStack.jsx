import { useUndoRedo } from "Hooks/useUndoRedo";
import { Btn, Line, Rows } from "UIKit";



export const CounterStack = () => {
    const {
        stack, push, data, current,
        undo, redo, isUndo, isRedo
    } = useUndoRedo(0);

    const handleAdd = () => {
        push(data + 1);
    }

    const renderStack = () => {
        return stack.map((i, index) => {
            if (current === index) {
                return <h2 key={index}>{i}</h2>
            }
            return <h4 key={index}>{i}</h4>
        })
    }

    return (
        <div>
            <Rows>
                <Line>{renderStack()}</Line>
                <h4>Count, {data}</h4>
                <Line>
                    <Btn onClick={handleAdd}>Add</Btn>
                    <Btn onClick={undo} disabled={!isUndo()}>Undo</Btn>
                    <Btn onClick={redo} disabled={!isRedo()}>Redo</Btn>
                </Line>
            </Rows>
        </div>
    )
}