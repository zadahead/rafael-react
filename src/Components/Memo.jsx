import { useCallback, useMemo, useState } from "react"
import { Btn, Line, Rows } from "UIKit";
import { MemoChild } from "./MemoChild";


export const Memo = () => {
    const [count, setCount] = useState(0);
    const [count2, setCount2] = useState(0);

    const name = "Mosh";

    const handleAdd2 = () => {
        setCount2(count2 + 1);
    }

    const handleAdd = useCallback(() => {
        setCount(count + 1);
    }, [count]);

    const expensive = () => {
        let mult = 1;
        for (let i = 0; i < 1000000; i++) {
            mult += count;
        }
        return mult;
    }

    const result = useMemo(expensive, [count])

    return (
        <div>
            <Rows>
                <Line>
                    <h3>Memo, {count}, {count2}, {result}</h3>
                    <Btn onClick={handleAdd}>Add</Btn>
                    <Btn onClick={handleAdd2}>Add2</Btn>
                </Line>
                <MemoChild name={name} onClick={handleAdd}></MemoChild>
            </Rows>
        </div>
    )
}