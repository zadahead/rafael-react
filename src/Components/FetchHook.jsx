import { Fetch, Rows } from "UIKit"


export const FetchHook = () => {

    const renderList = (list) => {
        return list.map(i => {
            return <h5 key={i.id}>{i.name}</h5>
        })
    }

    return (
        <div>
            <Rows>
                <h4>Users List:</h4>
                <Fetch url="/users" onLoad={renderList} />
            </Rows>
        </div>
    )
}

export const FetchTodos = () => {

    const renderList = (list) => {
        return list.splice(0, 10).map(i => {
            return <h5 key={i.id}>{i.title}</h5>
        })
    }

    return (
        <div>
            <Rows>
                <h4>Todos List:</h4>
                <Fetch url="/todos" onLoad={renderList} />
            </Rows>
        </div>
    )
}