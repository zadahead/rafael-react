import axios from "axios";
import { useEffect, useState } from "react";
import { Between, Icon, Line, Rows } from "UIKit";

export const UsersList = () => {
    const [list, setList] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        handleFetch();
    }, [])


    const handleFetch = async () => {
        const resp = await axios.get('https://jsonplaceholder.typicode.com/users');
        const data = resp.data;

        setTimeout(() => {
            setIsLoading(false);
            setList(data);
            console.log(data);
        }, 1000);
    }

    const handleDelete = (i) => {
        const filtered = list.filter(item => i !== item);
        setList(filtered);
    }

    const renderList = () => {
        return list.map(i => {
            return (
                <div key={i.id}>
                    <Between>
                        <Line>
                            <h3>{i.name}</h3>
                            <h4>{i.username}</h4>
                        </Line>
                        <Line>
                            <Icon i="trash" onClick={() => handleDelete(i)} />
                        </Line>
                    </Between>
                </div>
            )
        });
    }

    return (
        <div>
            <Rows>
                <h4>Users list</h4>
                <div>
                    {isLoading ? <h5>loading...</h5> : renderList()}
                </div>
            </Rows>
        </div>
    )
}