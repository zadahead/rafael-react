import { useState } from "react"
import { Btn, Input, Line, Rows } from "UIKit"

export const TwoWay = () => {
    const [value, setValue] = useState('hello mosh');

    const handleLog = () => {
        console.log(value);
    }

    const handleClear = () => {
        setValue('');
    }

    return (
        <div>
            <Rows>
                <Input placeholder="name" value={value} onChange={setValue} />
                <Line>
                    <Btn onClick={handleLog}>Log this</Btn>
                    <Btn onClick={handleClear}>clear</Btn>
                </Line>
            </Rows>
        </div>
    )
}