import { useInnerWidth } from "Hooks/useInnerWidth";


export const InnerWidth = () => {
    const width = useInnerWidth();

    return (
        <div>
            <h4>{width}</h4>
        </div>
    )
}