import { useEffect, useRef, useState } from "react";
import { Btn, Input, Rows } from "UIKit";


export const Refs = () => {
    const [value, setValue] = useState('hello');
    const inputRef = useRef();

    useEffect(() => {
        handleSelect();
    }, [])

    const handleSelect = () => {
        console.log('inputRef => ', inputRef);
        const input = inputRef.current;
        input.select();
    }

    return (
        <div>
            <Rows>
                <h3>Refs View</h3>
                <Input ref={inputRef} value={value} onChange={setValue} />
                <Btn onClick={handleSelect}>select</Btn>
            </Rows>
        </div>
    )
}