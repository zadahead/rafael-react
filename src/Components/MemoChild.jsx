import { memo } from 'react';
import { Btn } from 'UIKit';

const Wrap = (props) => {
    return (
        <div>
            <h4>MemoChild</h4>
            <Btn onClick={props.onClick}>Add</Btn>
        </div>
    )
}

export const MemoChild = memo(Wrap);