import { Rows } from "UIKit";


const numbersList = [1, 2, 3, 4, 5];

export const Numbers = () => {

    const renderList = () => {
        const mappedList = numbersList.map((item, index) => {
            return <h5 key={index}>{item}</h5>
        })

        return mappedList;
    }

    return (
        <div>
            <Rows>
                <h3>Numbers list</h3>
                <div>
                    {renderList()}
                </div>
            </Rows>
        </div>
    )
}