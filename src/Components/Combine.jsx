import { useCounter } from "Hooks/useCounter";
import { useSwitcher } from "Hooks/useSwitcer";
import { Btn, Line, Rows } from "UIKit"

export const Combine = () => {
    const { isOn, handleSwitch } = useSwitcher();
    const [count, handleAdd] = useCounter();


    const boxStyle = {
        width: '100px',
        height: '100px',
        backgroundColor: isOn ? 'red' : 'blue'
    }

    return (
        <div>
            <Rows>
                <div style={boxStyle}>
                    <h1>{count}</h1>
                </div>
                <Line>
                    <Btn onClick={handleAdd}>Add</Btn>
                    <Btn onClick={handleSwitch}>Switch</Btn>
                </Line>
            </Rows>
        </div>
    )
}