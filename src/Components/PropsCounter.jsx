import { Btn } from "UIKit"


/*
 1) initial value
 2) current value
 3) set value
 4) re-render
*/

export const PropsCounter = (props) => {

    return (
        <div>
            <h3>Count, {props.count}</h3>
            <Btn onClick={props.handleAdd}>Add</Btn>
        </div>
    )
}