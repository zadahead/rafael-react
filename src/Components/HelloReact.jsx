import { HelloRafael } from "./HelloRafael";

export const HelloReact = () => {
    return (
        <>
            <h2>Hello React</h2>
            <div>
                <HelloRafael name="Mosh" age={5}>
                    <h2>Some child element</h2>
                </HelloRafael>
            </div>
        </>
    )
}