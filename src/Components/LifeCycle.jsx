import { useEffect, useState } from "react";
import { Rows } from "UIKit";

export const LifeCycle = () => {
    const [time, setTime] = useState('loaded');
    const [count, setCount] = useState(0);

    const [isLoading, setIsLoading] = useState(true);

    // console.log('render');
    useEffect(() => {
        console.log('mounted');
        setTimeout(() => {
            setIsLoading(false);
        }, 2000);



        return () => {
            console.log('will unmount');
        }

    }, []); // [] == when mounted for the first time.

    useEffect(() => {
        if (!isLoading) {
            setTimeout(() => {
                setTime(new Date().toLocaleTimeString());
            }, 1000);
        }
        console.log('updated + mounted', isLoading);
    }); //no dependency == on every set state (+ mounted);

    useEffect(() => {
        console.log('updated', count);
        document.body.addEventListener('click', handleBodyClick);

        return () => {
            document.body.removeEventListener('click', handleBodyClick);
            //  console.log('will update', count);
        }
    }, [count])

    const handleBodyClick = () => {
        //console.log('handleBodyClick', count);
        setCount(count + 1);
    }

    return (
        <div>
            <Rows>
                {isLoading ? <h4>loading.....</h4> : <h4>{time}</h4>}
                <h4>Life Cycle, {count}</h4>
            </Rows>
        </div>
    )
}