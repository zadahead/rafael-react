import { Rows } from "UIKit";

export const Conditional = (props) => {

    const renderYoyo = () => {
        if (props.isTrue) {
            return <h4>Yoyoyo</h4>
        }
        return null;
    }

    return (
        <div>
            <Rows>
                <h3>Hello Mosh, {props.isTrue ? 'true' : 'false'}</h3>
                {renderYoyo()}
            </Rows>
        </div>
    )
}