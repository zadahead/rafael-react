
import './App.css';

import { Grid, Line, Between, Icon, Inner, Btn } from 'UIKit';

import { Routes, Route, NavLink } from 'react-router-dom';
import { StatesView } from 'Views/StatesView';
import { CycleView } from 'Views/CycleView';
import { UsersView } from 'Views/UsersView';
import { RefsView } from 'Views/RefsView';
import { DropDownView } from 'Views/DropDownView';
import { CustomHooksView } from 'Views/CustomHooksView';
import { useContext } from 'react';
import { counterContext } from 'Context/counterContext';
import { themeContext } from 'Context/themeContext';

import { useSelector } from "react-redux";
import { TodosView } from 'Views/TodosView';
import { HomeView } from 'Views/HomeView';
import { LoginView } from 'Views/LoginView';
import { SettingsView } from 'Views/SettingsView';
import { userContext } from 'Auth/userContext';
import { PrivateRoute } from 'Auth/PrivateRoute';

const App = () => {
    const { color } = useContext(themeContext);
    const { isLogin, logout, user, toLogin } = useContext(userContext);

    console.log('user', user);


    return (
        <div className='App' theme={color}>
            <Grid>
                <div>
                    <Inner>
                        <Between>
                            <Line>
                                <NavLink to="/home">Home</NavLink>
                                <NavLink to="/states">State</NavLink>
                                <NavLink to="/cycle">Cycle</NavLink>
                                <NavLink to="/users">Users</NavLink>
                                <NavLink to="/refs">Refs</NavLink>
                                <NavLink to="/dropdown">Dropdown</NavLink>
                                <NavLink to="/hooks">Hooks</NavLink>
                                <NavLink to="/todos">Todos</NavLink>
                            </Line>

                            {isLogin && (
                                <Line>
                                    <h4>{user.username}</h4>
                                    <NavLink to="/settings">
                                        <Icon i="cog" />
                                    </NavLink>
                                    <Btn onClick={logout}>Logout</Btn>

                                </Line>
                            )}

                            {!isLogin && (
                                <Routes>
                                    <Route path='/login' element={<></>} />
                                    <Route path='/*' element={<Btn onClick={toLogin}>Login</Btn>} />
                                </Routes>
                            )}
                        </Between>
                    </Inner>
                </div>
                <div>
                    <Inner>
                        <Routes>
                            <Route path='/*' element={<HomeView />} />
                            <Route path='/states' element={<StatesView />} />
                            <Route path='/cycle' element={<CycleView />} />
                            <Route path='/users' element={<UsersView />} />
                            <Route path='/refs' element={<RefsView />} />
                            <Route path='/dropdown' element={<DropDownView />} />
                            <Route path='/hooks' element={<CustomHooksView />} />
                            <Route path='/todos' element={<TodosView />} />
                            <Route path='/login' element={<LoginView />} />
                            <Route path='/settings' element={<PrivateRoute child={<SettingsView />} />} />
                        </Routes>
                    </Inner>
                </div>
            </Grid>
        </div>
    )
}

export default App;