import { Combine } from "Components/Combine"
import { CounterHook } from "Components/CounterHook"
import { FetchHook, FetchTodos } from "Components/FetchHook"
import { InnerWidth } from "Components/InnerWidth"
import { SwitcherHook } from "Components/SwitchHook"
import { Box } from "UIKit"

export const CustomHooksView = () => {
    return (
        <div>
            <Box title="Fetch Todos">
                <FetchTodos />
            </Box>
            <Box title="Fetch Users">
                <FetchHook />
            </Box>
            <Box title="Inner Width">
                <InnerWidth />
            </Box>
            <Box title="useCount">
                <CounterHook />
            </Box>
            <Box title="useSwitcher">
                <SwitcherHook />
            </Box>
            <Box title="Combine Hooks">
                <Combine />
            </Box>
        </div>
    )
}