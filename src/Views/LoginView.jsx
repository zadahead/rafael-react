import { userContext } from "Auth/userContext";
import { useInput } from "Hooks/useInput";
import { useValidator } from "Hooks/useValidator";
import { useContext } from "react"
import { useNavigate } from "react-router-dom";
import { Box, Btn, Center, Input, RowsFlex } from "UIKit";



export const LoginView = () => {
    const username = useInput('', 'username');
    const password = useInput('', 'password');

    const { login } = useContext(userContext);
    const navigate = useNavigate();

    const validate = useValidator([
        [username, ['empty', 'email']],
        [password, ['empty']]
    ]);


    const handleLogin = () => {
        if (validate()) {
            console.log('handleLogin');
            login({
                username: username.value,
                token: 'ASDASDASD'
            })
            navigate('/home');
        }
    }

    return (
        <Center>
            <Box title="Login">
                <RowsFlex>
                    <Input {...username} />
                    <Input {...password} />
                    <Center>
                        <Btn onClick={handleLogin}>Login</Btn>
                    </Center>
                </RowsFlex>
            </Box>
        </Center>
    )
}