import { UsersList } from "Components/UsersList"
import { Box } from "UIKit"

export const UsersView = () => {
    return (
        <div>
            <Box title="Users List">
                <UsersList />
            </Box>
        </div>
    )
}