import { useState } from "react";
import { Box, Dropdown } from "UIKit";

const list = [
    { id: 1, name: 'Mosh' },
    { id: 2, name: 'David' },
    { id: 3, name: 'Other' }
]

export const DropDownView = () => {
    const [selected, setSelected] = useState(null);

    return (
        <div>
            <Box title="drop down">
                <Dropdown
                    list={list}
                    selected={selected}
                    onChange={setSelected}
                />
            </Box>
        </div>
    )
}