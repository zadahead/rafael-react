import { BasicClass } from "Components/Classes/BasicClass";
import { CounterClass } from "Components/Classes/CounterClass";
import { CounterStack } from "Components/CounterStack";
import { Memo } from "Components/Memo";
import { ReduxCounter } from "Components/ReduxCounter";
import { counterContext } from "Context/counterContext";
import { themeContext } from "Context/themeContext";
import { useUndoRedo } from "Hooks/useUndoRedo";
import { useContext, useState } from "react";
import { Box, Btn, Dropdown, Icon, Line, Rows, RowsFlex } from "UIKit"
import { Tree } from "UIKit/Widgets/Tree/Tree";
import _ from 'lodash';

const tree = [
    {
        id: 1,
        title: 'mosh'
    },
    {
        id: 2,
        title: 'david',
        open: false,
        items: [
            {
                id: 3,
                title: 'inner'
            }
        ]
    },
    {
        id: 4,
        title: 'rafael',
        open: true,
        items: [
            {
                id: 5,
                title: 'rafael inner'
            }
        ]
    }
]

export const HomeView = () => {
    const {
        stack, push, data, current,
        undo, redo, isUndo, isRedo
    } = useUndoRedo(tree);

    const { count } = useContext(counterContext);
    const { list, selected, onChange } = useContext(themeContext);

    const handleClick = () => {
        console.log('hello mosh');
    }

    const handleTreeItemSelect = (item) => {
        console.log(item);
    }

    const renderStack = () => {
        return stack.map((i, index) => {
            if (current === index) {
                return <h2 key={index}>{index}</h2>
            }
            return <h4 key={index}>{index}</h4>
        })
    }

    const handlePush = (list) => {
        push(_.cloneDeep(list));
    }


    return (
        <>
            <RowsFlex>
                <Box title="Undo Redo">
                    <CounterStack />
                </Box>
                <Box title="Files Tree">
                    <Rows>
                        <Line>{renderStack()}</Line>
                        <Tree list={data} onChange={handlePush} onItemSelect={handleTreeItemSelect} />
                        <Line>
                            <Btn onClick={undo} disabled={!isUndo()}>Undo</Btn>
                            <Btn onClick={redo} disabled={!isRedo()}>Redo</Btn>
                        </Line>
                    </Rows>

                </Box>
                <Box title="Class Components">
                    <Rows>
                        <CounterClass />
                        <BasicClass />
                    </Rows>
                </Box>
                <Box title="Redux Toolkit">
                    {/* <ReduxCounter /> */}
                </Box>
                <Box title="memo">
                    <Memo />
                </Box>
                <Box title="Overview">
                    <Dropdown list={list} selected={selected} onChange={onChange} />


                    <Icon i="star" />
                    <h2>Hello React App, {count}</h2>

                    <Btn i="heart" onClick={handleClick}>Click Me</Btn>
                </Box>

            </RowsFlex>
        </>
    )
}