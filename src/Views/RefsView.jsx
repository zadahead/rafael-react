import { Refs } from "Components/Refs";
import { useRef, useState } from "react";
import { Box, Btn, Input } from "UIKit";


export const RefsView = () => {
    const [value, setValue] = useState('hello');

    return (
        <div>
            <Box title="Refs view">
                <Refs />
            </Box>
        </div>
    )
}