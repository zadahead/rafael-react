import { useSelector, useDispatch } from "react-redux";
import { LifeCycle } from "Components/LifeCycle"
import { Box, Btn, Line } from "UIKit"
import { addCount, multCount, reduCount } from "State/counter";
import { colorSwitch } from "State/color";

export const CycleView = () => {
    const store = useSelector(state => state);

    const dispatch = useDispatch();

    console.log(store);

    const handleAdd = () => {
        dispatch(addCount())
    }

    const handleReduce = () => {
        dispatch(reduCount(5))
    }

    const handleMultiply = () => {
        dispatch(multCount(5))
    }

    const handleSwitchColor = () => {
        dispatch(colorSwitch());
    }

    return (
        <div>
            <Box title="Life Cycle">
                <h4>Redux Count: {store.hello}</h4>
                <Line>
                    <Btn onClick={handleAdd}>Add</Btn>
                    <Btn onClick={handleReduce}>Reduce</Btn>
                    <Btn onClick={handleMultiply}>Multiply</Btn>
                    <Btn onClick={handleSwitchColor}>Switch Color</Btn>
                </Line>
                <LifeCycle />
            </Box>
        </div>
    )
}