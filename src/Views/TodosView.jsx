import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import { deleteTodos, getTodos, patchTodos, postTodos } from "State/todos";
import { Btn, Input, Line, Rows } from "UIKit";
import { ListItem } from "UIKit/Widgets/ListItem/ListItem";

export const TodosView = () => {
    const [value, setValue] = useState('');
    const dispatch = useDispatch();
    const list = useSelector(store => store.todos);

    useEffect(() => {
        dispatch(getTodos());
    }, []);

    const handleUpdateChecked = (id) => {
        dispatch(patchTodos(id));
    }

    const handleDelete = (id) => {
        dispatch(deleteTodos(id));
    }

    const handleAdd = () => {
        if (value) {
            dispatch(postTodos(value));
            setValue('')
        }
    }

    console.log(list);

    return (
        <div>
            <Rows>
                <h3>TodosView</h3>
                <Line>
                    <Input value={value} onChange={setValue} placeholder="new taks" />
                    <Btn onClick={handleAdd}>Add</Btn>
                </Line>
                <div>
                    {[...list].reverse().map(i => {
                        return (
                            <ListItem
                                key={i.id}
                                title={i.title}
                                checked={i.completed}
                                onChecked={() => handleUpdateChecked(i.id)}
                                onDelete={() => handleDelete(i.id)}
                            />
                        )
                    })}
                </div>
            </Rows>
        </div>
    )
}