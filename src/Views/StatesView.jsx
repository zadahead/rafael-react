import { AsyncState } from "Components/AsyncState";
import { ColorSwitcher } from "Components/ColorSwitch";
import { Conditional } from "Components/Conditional";
import { Counter } from "Components/Counter";
import { Cubes } from "Components/Cubes";
import { Numbers } from "Components/Number";
import { PropsCounter } from "Components/PropsCounter";
import { Toggler } from "Components/Toggler";
import { TwoWay } from "Components/TwoWay";
import { Users } from "Components/Users";
import { useState } from "react";
import { Box, Btn, Checkbox, Rows } from "UIKit";

const usersList = [
    { id: 1, name: 'item 1' },
    { id: 2, name: 'item 2' },
    { id: 3, name: 'item 3' },
    { id: 4, name: 'item 4' }
];

export const StatesView = () => {
    const [isTrue, setIsTrue] = useState(false);

    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleShowSelection = () => {
        console.log(isTrue);
    }

    return (
        <div>
            <Box title="Async State">
                <AsyncState />
            </Box>
            <Box title="Cubes">
                <Cubes />
            </Box>
            <Box title="Counter">
                <Counter />
            </Box>
            <Box title="Users List">
                <Users list={usersList} />
            </Box>
            <Box title="Checkbox">
                <Rows>
                    <Checkbox checked={isTrue} onChange={setIsTrue} label="is true" />
                    <Btn onClick={handleShowSelection}>Show Selection</Btn>
                </Rows>
            </Box>
            <Box title="Numbers">
                <Numbers />
            </Box>
            <Box title="Toggler">
                <Toggler>
                    <PropsCounter count={count} handleAdd={handleAdd} />
                </Toggler>
            </Box>
            <Box title="Conditional Rendering">
                <Conditional isTrue={isTrue} />
            </Box>
            <Box title="Two way">
                <TwoWay />
            </Box>

            <Box title="Color Switcher">
                <ColorSwitcher />
            </Box>

        </div>
    )
}