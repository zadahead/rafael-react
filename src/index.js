import React from 'react';
import ReactDOM from 'react-dom/client';

import { BrowserRouter as Router } from 'react-router-dom';

import App from 'App';
import "./index.css";

import { CountContext } from 'Context/countContext';
import { CounterCountext } from 'Context/counterContext';
import { ThemeContext } from 'Context/themeContext';
import { Store } from 'State/store';
import { UserProvider } from 'Auth/userContext';
import { Redux } from 'Redux/store';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Router>
    <Store>
      <UserProvider>
        <CountContext>
          <CounterCountext>
            <ThemeContext>
              <App />
            </ThemeContext>
          </CounterCountext>
        </CountContext>
      </UserProvider>
    </Store>
  </Router>
);


