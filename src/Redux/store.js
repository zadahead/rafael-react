import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import countReducer from "./counter";

const store = configureStore({
    reducer: {
        counter: countReducer
    }
})

export const Redux = ({ children }) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}