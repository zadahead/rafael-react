import { createContext } from "react";

export const countContext = createContext();

const { Provider } = countContext;

export const CountContext = ({ children }) => {
    const count = 18;
    const hello = "hello mosh";

    return (
        <Provider value={{ count, hello }}>
            {children}
        </Provider>
    )
}

//helloMoshContext
