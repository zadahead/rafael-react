import { createContext, useState } from "react";

export const themeContext = createContext();

const { Provider } = themeContext;

const colors = [
    { id: 1, name: 'red' },
    { id: 2, name: 'blue' },
    { id: 3, name: 'primary' }
]

export const ThemeContext = ({ children }) => {
    const [selected, setSelected] = useState(3);

    const getColor = () => {
        const color = colors.find(i => i.id === selected);
        return color.name;
    }

    const resp = {
        color: getColor(),
        list: colors,
        onChange: setSelected,
        selected: selected
    }

    return (
        <Provider value={resp}>
            {children}
        </Provider>
    )
}