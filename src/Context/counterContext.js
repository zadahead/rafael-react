import { createContext, useState } from "react";

export const counterContext = createContext();

const { Provider } = counterContext;

export const CounterCountext = ({ children }) => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <Provider value={{ count, handleAdd }}>
            {children}
        </Provider>
    )
}