/// <reference types="cypress" />

describe("Login Page", () => {
    it("will visit /login", () => {
        cy.visit('http://localhost:3000/login');
    })

    it("will prevent login on button click", () => {

        cy.get('[data-testid="btn"]').click();


        cy.get('.Input').eq(0).should('have.class', 'error');
        cy.get('.Input').eq(1).should('have.class', 'error');
    })

    it("will allow password, and not username", () => {

        cy.get('.Input').eq(0).type('hello');
        cy.get('.Input').eq(1).type('mypass');


        cy.get('[data-testid="btn"]').click();


        cy.get('.Input').eq(0).should('have.class', 'error');
        cy.get('.Input').eq(1).should('not.have.class', 'error');
    })

    it("will allow login", () => {

        cy.get('.Input').eq(0).type('hello@mosh.com');
        cy.get('.Input').eq(1).type('mypass');


        cy.get('.Input').eq(0).should('not.have.class', 'error');
        cy.get('.Input').eq(1).should('not.have.class', 'error');

        cy.get('[data-testid="btn"]').click();
    })

    it("will go to home page after login", () => {
        cy.location().should((loc) => {
            expect(loc.pathname).to.eq('/home');
        })
    })
})